//
//  ViewController2.swift
//  EjemploCapacitación
//
//  Created by Natalia Martin on 2/10/18.
//  Copyright © 2018 Ejemplo. All rights reserved.
//

import Foundation
import UIKit

class ViewController2: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let items = ["celda 1", "celda 2"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cellReuseIdentifier")
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
}

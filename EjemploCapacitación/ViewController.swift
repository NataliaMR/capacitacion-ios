//
//  ViewController.swift
//  EjemploCapacitación
//
//  Created by Natalia Martin on 20/9/18.
//  Copyright © 2018 Ejemplo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var helloWorldTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didPressHelloWorldBtn() {
        helloWorldTF.text = "Hello World!"
        
        let alert = UIAlertController(title: "Hello World!", message: "Hola Mundo!", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}

